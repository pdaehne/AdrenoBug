package com.example.adrenobug;

import android.opengl.GLES30;
import android.opengl.GLSurfaceView.Renderer;
import android.util.Log;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import javax.microedition.khronos.opengles.GL10;
import javax.microedition.khronos.egl.EGLConfig;

class MyGLRenderer implements Renderer {

    private static final String TAG = MainActivity.class.getSimpleName();

    private static final float vertices[] = {
            -1, 1, 1, 1, -1, -1, 1, -1
    };

    private static final short indices[] = { 0, 2, 1, 1, 2, 3 };

    private static final float red[] = { 1, 0, 0, 1 };

    private static final float green[] = { 0, 1, 0, 1 };

    private static final String vertexShaderSrc =
            "#version 300 es\n" +
                    "layout(location = 0) in vec4 a_vertex;\n" +
                    "void main(void) {\n" +
                    "  gl_Position = a_vertex;\n" +
                    "}\n";

    private static final String  fragmentShaderSrc =
            "#version 300 es\n" +
                    "precision mediump float;\n" +
                    "layout(location = 0) out vec4 fragColor;\n" +
                    "layout(std140) uniform color_ubo {\n" +
                    "  vec4 color;\n" +
                    "};\n" +
                    "void main(void) {\n" +
                    "  fragColor = color;\n" +
                    "}\n";

    private int program;

    private int uboIndex;

    public void onSurfaceCreated(GL10 unused, EGLConfig config) {
        Log.d(TAG, "vendor = " + GLES30.glGetString(GLES30.GL_VENDOR));
        Log.d(TAG, "renderer = " + GLES30.glGetString(GLES30.GL_RENDERER));

        int[] buffers = new int[4];
        GLES30.glGenBuffers(4, buffers, 0);

        // Create vertex buffer
        ByteBuffer bb = ByteBuffer.allocateDirect(vertices.length * 4);
        bb.order(ByteOrder.nativeOrder());
        FloatBuffer vertexBuffer = bb.asFloatBuffer();
        vertexBuffer.put(vertices);
        vertexBuffer.position(0);
        GLES30.glBindBuffer(GLES30.GL_ARRAY_BUFFER, buffers[0]);
        GLES30.glBufferData(GLES30.GL_ARRAY_BUFFER, vertexBuffer.capacity() * 4, vertexBuffer, GLES30.GL_STATIC_DRAW);
        GLES30.glEnableVertexAttribArray(0);
        GLES30.glVertexAttribPointer(0, 2, GLES30.GL_FLOAT, false, 0, 0);

        // Create index buffer
        bb = ByteBuffer.allocateDirect(indices.length * 2);
        bb.order(ByteOrder.nativeOrder());
        ShortBuffer indexBuffer = bb.asShortBuffer();
        indexBuffer.put(indices);
        indexBuffer.position(0);
        GLES30.glBindBuffer(GLES30.GL_ELEMENT_ARRAY_BUFFER, buffers[1]);
        GLES30.glBufferData(GLES30.GL_ELEMENT_ARRAY_BUFFER, indexBuffer.capacity() * 2, indexBuffer, GLES30.GL_STATIC_DRAW);

        // Create UBO with red color
        bb = ByteBuffer.allocateDirect(red.length * 4);
        bb.order(ByteOrder.nativeOrder());
        FloatBuffer redBuffer = bb.asFloatBuffer();
        redBuffer.put(red);
        redBuffer.position(0);
        GLES30.glBindBufferBase(GLES30.GL_UNIFORM_BUFFER, 0, buffers[2]);
        GLES30.glBufferData(GLES30.GL_UNIFORM_BUFFER, redBuffer.capacity() * 4, redBuffer, GLES30.GL_STATIC_DRAW);

        // Create UBO with green color
        bb = ByteBuffer.allocateDirect(green.length * 4);
        bb.order(ByteOrder.nativeOrder());
        FloatBuffer greenBuffer = bb.asFloatBuffer();
        greenBuffer.put(green);
        greenBuffer.position(0);
        GLES30.glBindBufferBase(GLES30.GL_UNIFORM_BUFFER, 1, buffers[3]);
        GLES30.glBufferData(GLES30.GL_UNIFORM_BUFFER, greenBuffer.capacity() * 4, greenBuffer, GLES30.GL_STATIC_DRAW);

        // Create vertex shader
        int vertexShader = GLES30.glCreateShader(GLES30.GL_VERTEX_SHADER);
        GLES30.glShaderSource(vertexShader, vertexShaderSrc);
        GLES30.glCompileShader(vertexShader);
        Log.d(TAG, "vertexShaderInfoLog = " + GLES30.glGetShaderInfoLog(vertexShader));

        // Create fragment shader
        int fragmentShader = GLES30.glCreateShader(GLES30.GL_FRAGMENT_SHADER);
        GLES30.glShaderSource(fragmentShader, fragmentShaderSrc);
        GLES30.glCompileShader(fragmentShader);
        Log.d(TAG, "fragmentShaderInfoLog = " + GLES30.glGetShaderInfoLog(fragmentShader));

        // Create program
        program = GLES30.glCreateProgram();
        GLES30.glAttachShader(program, vertexShader);
        GLES30.glAttachShader(program, fragmentShader);
        GLES30.glLinkProgram(program);
        Log.d(TAG, "programInfoLog = " + GLES30.glGetProgramInfoLog(program));
        GLES30.glUseProgram(program);
        uboIndex = GLES30.glGetUniformBlockIndex(program, "color_ubo");
    }

    public void onDrawFrame(GL10 unused) {
        GLES30.glClear(GLES30.GL_COLOR_BUFFER_BIT);
        GLES30.glUniformBlockBinding(program, uboIndex, 0);
        GLES30.glDrawElements(GLES30.GL_TRIANGLES, 3, GLES30.GL_UNSIGNED_SHORT, 0);
        GLES30.glUniformBlockBinding(program, uboIndex, 1);
        GLES30.glDrawElements(GLES30.GL_TRIANGLES, 3, GLES30.GL_UNSIGNED_SHORT, 6);

        Log.d(TAG, "error = " + GLES30.glGetError());
    }

    public void onSurfaceChanged(GL10 unused, int width, int height) {
        GLES30.glViewport(0, 0, width, height);
    }
}
